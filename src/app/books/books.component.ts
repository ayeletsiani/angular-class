import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BooksService } from '../books.service';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  panelOpenState = false;
  books:any;
  books$:Observable<any>;
  userId:string;

  constructor(private booksService:BooksService,  public auth:AuthService) { }
  
  ngOnInit() {
    
    //this.books$ = this.booksService.getBooks();
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        this.books$ = this.booksService.getBooks(this.userId);
      }
    )
  }

  deleteBook(bookId:string){
    this.booksService.deleteBook(this.userId,bookId);
  }

}
