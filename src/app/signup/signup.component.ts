import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from './../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  hide = true;
  email:string;
  password:string;
 
  constructor(private auth: AuthService, private router: Router, private route:ActivatedRoute) { }

  onSubmit(){
    this.auth.signUp(this.email,this.password);
  }
  
  ngOnInit() {
  }

}
