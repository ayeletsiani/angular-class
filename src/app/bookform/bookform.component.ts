import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from '../books.service';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {
  buttontext:string = 'Add';
  pagetitle:string="Add New Book";
  title:string;
  author:string;
  isEdit:boolean = false;
  id: string;
  userId:string;

  constructor(private router: Router, 
              private route:ActivatedRoute,
              private bookservice:BooksService, 
              private auth:AuthService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.bookid;
    this.auth.user.subscribe(
      user => {
        this.userId = user.uid;
        if(this.id)
    {
      this.isEdit = true;
      this.buttontext = "Update";
      this.pagetitle = "Edit Book";
      this.bookservice.getBook(this.userId,this.id).subscribe(
        book => {
          console.log(this.title);
          this.author=book.data().author;
          this.title=book.data().title;
        }
      )
    }
      }
    )
    
  
  }

  onSubmit(){
    if(this.isEdit) {
        this.bookservice.updateBook(this.userId,this.id, this.title,this.author);
        this.router.navigate(['/books'])
      }
    
      else{ 
        this.bookservice.addBook(this.userId, this.title,this.author);
        this.router.navigate(['/books'])
      }
    
}


}
