import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Temp } from './interfaces/temp';
import { TempRaw } from './interfaces/temp-raw';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';  
import { map, catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class TempService {
  private URL ="http://api.openweathermap.org/data/2.5/weather?q=";
  private KEY ="c2addb75777e24415bc0705646e8a194";
  private IMP ="&units=metric";

  constructor(private http:HttpClient) { }

  searchWeatherData(cityName: String):Observable<Temp>{
        return this.http.get<TempRaw>(`${this.URL}${cityName}&APPID=${this.KEY}${this.IMP}`)
        .pipe(map(data => this.transformWeatherData(data)),
        catchError(this.handleError))
      }
  
  private transformWeatherData(data:TempRaw):Temp 
  {
        return {
            name:data.name,
            country:data.sys.country,
            image: `http://api.openweathermap.org/img/w/${data.weather[0].icon}`,
            description: data.weather[0].description,
            temperature: data.main.temp,
            lat:data.coord.lat,
            lon:data.coord.lon
    
      }
    
   }
    
      private handleError(res:HttpErrorResponse){
        console.log(res.error);
        return throwError(res.error)
      }
     
     
    


}
