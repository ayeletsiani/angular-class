import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { User } from './interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<User | null>
  constructor(public afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState //פייר בייס ינהל את היוזרים
  }

  signUp(email:string, password:string)
  {
    this.afAuth
        .auth.createUserWithEmailAndPassword(email,password)
        .then(() => {
          this.router.navigate(['/books']);
        }) //"then" הפונקציה 
                                                            //מחזירה פורמיס
                                                            // כמו סאבסקרייב של אובזראבל
  }

 logOut(){
   this.afAuth.auth.signOut().then(res => console.log('Succesful Logout', res));
 }

 login(email:string,password:string){
  console.log(email);

   this.afAuth.auth.signInWithEmailAndPassword(email,password).then(
    () => {
    this.router.navigate(['/books']);
  })



}
}
