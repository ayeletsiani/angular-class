import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {
  
  //הגדרת משתנה מסוג "אוסף"  י 
  userCollection:AngularFirestoreCollection= this.db.collection('users');
  bookCollection:AngularFirestoreCollection;
  //books = [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}];
  
  constructor(private db:AngularFirestore) { }
  
  getBooks(userId:string):Observable<any[]>{
    //return this.db.collection('books').valueChanges({idField:'id'}); 
    this.bookCollection = this.db.collection(`users/${userId}/books`);
    return this.bookCollection.snapshotChanges().pipe(
      map(
       collection => collection.map(
         document=> {
           const data = document.payload.doc.data();
           data.id = document.payload.doc.id;
           return data;
         }
       ) 
      )
    )
   }

   addBook(userId:string, title:string,author:string){
    const book = {title:title,author:author};
    //this.db.collection('books').add(book);
    this.userCollection.doc(userId).collection('books').add(book); //הוספה של הספר בתוך האוסף של היוזרים

  }

  deleteBook(userId:string, bookId:string) 
  {
      this.db.doc(`users/${userId}/books/${bookId}`).delete();
  }

  updateBook(userId:string,bookId:string, title:string,author:string){
    const book= {title:title,author:author};
    this.db.doc(`users/${userId}/books/${bookId}`).update(book);
  }

  getBook(userId:string, id:string):Observable<any>{
    return this.db.doc(`users/${userId}/books/${id}`).get();
  }












}
  /* getBooks(){
        const bookObservable = new Observable (
          observer => {
            setInterval(
             ()=> observer.next(this.books),3000
            )
          }
         )
         return bookObservable;
        }
    */
  /*addBooks()
  {
      setInterval(
         ()=> this.books.push({title: 'A new book', author:'new author'})
         ,5000)
         
  } */

  

