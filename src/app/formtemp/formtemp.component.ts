import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-formtemp',
  templateUrl: './formtemp.component.html',
  styleUrls: ['./formtemp.component.css']
})
export class FormtempComponent implements OnInit {

  constructor(private router: Router) { }
  temper: number;
  city: string;
  
  cities:object[] = [{id:1, name:'Tel Aviv'},{id:2, name:'London'},{id:3, name:'Paris'}];

  ngOnInit() {
  }
  
  onSubmit(){
    this.router.navigate(['/temperature', this.temper, this.city]);
  }


}
