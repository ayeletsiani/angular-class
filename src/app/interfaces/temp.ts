export interface Temp {
   
        name:String,
        country:String,
        image:string,
        description:String,
        temperature:number,
        lat?:number,
        lon?:number
    
}
