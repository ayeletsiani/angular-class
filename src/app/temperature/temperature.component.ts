import { Component, OnInit } from '@angular/core';

//URL בכדי למשוך מידע משורת ה  
import { ActivatedRoute } from '@angular/router'; 
import { TempService } from '../temp.service';
import { Observable } from 'rxjs';
import { Temp } from '../interfaces/temp';


@Component({
  selector: 'app-temperature',
  templateUrl: './temperature.component.html',
  styleUrls: ['./temperature.component.css']
})
export class TemperatureComponent implements OnInit {

  constructor(private route: ActivatedRoute, private tempservice:TempService) { }
  
  temp: number;
  city: string;
  likes: number = 0;
  tempData$:Observable<Temp>;
  image;
  errorMessage:string;
  hasError:boolean;

  
  ngOnInit() {
      // משיכת המידע המגיע משורת האינטרנט
     // this.temp = this.route.snapshot.params.temp;

      // מתייחס לשם שהוגדר בראוטאר בקובץ המודל _city 
      this.city= this.route.snapshot.params._city;
      
      this.tempData$ = this.tempservice.searchWeatherData(this.city);
      this.tempData$.subscribe(
        data => {
          console.log(data);
          this.temp = data.temperature;
          this.image = data.image;
        },
        error=> {
          this.hasError = true;
          this.errorMessage = error.message;
          console.log('in the component '+ error.message);
        }
      )
  }
  
  likeUp(){
    this.likes ++;
  }
}
